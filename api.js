/*
 * Copyright (c) 2019 Nord Pool. All rights reserved.
 */

"use strict";

const _ = require("lodash");
const util = require("util");
const moment = require("moment");
const request = require("request");
const HTTP_STATUS = require("http-status");
const config = require("./options.js");
const handler = require("./responseHandler.js");
const today = moment()
  .utc()
  .startOf("day")
  .format();
const yesterday = moment()
  .utc()
  .startOf("day")
  .subtract(1, "day")
  .format();

login().then(token =>
  getAuctionsByCloseForBidding(token, yesterday, today)
    .then(response => {
      print(response);
      const cweAuction = _.find(response.body, a => a.id.startsWith("CWE"));
      return getOrdersByAuction(token, cweAuction.id);
    })
    .then(response => {
      print(response);
    })
);

function print(response) {
  const toPrint = util.inspect(response.body, {
    showHidden: false,
    depth: 3,
    maxArrayLength: 3
  });
  console.log("\nResponse body received from", response.url);
  console.log(toPrint);
}

function login() {
  const options = config.ssoConfig;
  return new Promise((resolve, reject) =>
    request.post(options, (err, res, body) => {
      if (err) reject(err);
      if (res.statusCode !== HTTP_STATUS.OK) {
        reject("ERROR IN LOGIN.\n" + JSON.stringify(res, null, "\t"));
      } else {
        resolve(JSON.parse(body).access_token);
      }
    })
  ).catch(e => console.log("Login failed with error:\n", e));
}

function getAuctionsByCloseForBidding(token, from, to) {
  const options = {
    url: config.publicBaseUrl + config.endpoints.auctions,
    headers: {
      "Content-Type": "application/json",
      authorization: "Bearer " + token
    },
    qs: {
      closeBiddingFrom: from,
      closeBiddingTo: to
    }
  };
  return new Promise(resolve => request(options, handler(resolve)));
}

function getOrdersByAuction(token, auctionId) {
  const options = {
    url:
      config.publicBaseUrl +
      config.endpoints.orders.replace("{auctionId}", auctionId),
    headers: {
      "Content-Type": "application/json",
      authorization: "Bearer " + token
    }
  };
  return new Promise(resolve => request(options, handler(resolve)));
}
