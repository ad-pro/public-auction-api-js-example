/*
 * Copyright (c) 2019 Nord Pool. All rights reserved.
 */

"use strict";

const clientId = "client_auction_api";
const clientSecret = "client_auction_api";

module.exports = {
  publicBaseUrl: "https://auctions-api.test.nordpoolgroup.com",
  endpoints: {
    auctions: "/api/v1/auctions",
    orders: "/api/v1/auctions/{auctionId}/orders"
  },
  ssoConfig: {
    url: "https://sts.test.nordpoolgroup.com/connect/token",
    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Basic " + Buffer.from(clientId + ":" + clientSecret).toString("base64")
    },
    form: {
      scope: "auction_api",
      clientId: clientId,
      clientSecret: clientSecret,
      username: "...",
      password: "...",
      grant_type: "password"
    }
  }
};
