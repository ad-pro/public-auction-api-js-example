# public-auction-api-js-example

 1. Clone repository on your machine.
 2. Install NodeJS https://nodejs.org/en/download/
 3. Open command line in root repository folder.
 4. Execute ```npm install```
 5. Open options.js and set your API username and password.
 6. Execute ```node api```

You will see responses of 2 API calls logged in command line window.
