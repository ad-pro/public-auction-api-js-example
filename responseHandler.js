/*
* Copyright (c) 2019 Nord Pool. All rights reserved.
*/

"use strict";

const HTTP_STATUS = require("http-status");
const _ = require("lodash");

const formResponse = body => {
  if (_.isEmpty(body)) {
    return body;
  }
  if (_.isObject(body)) {
    return body;
  }
  try {
    return JSON.parse(body);
  } catch (err) {
    return body;
  }
};

const resolveHandler = resolve => {
  return function callback(err, res, body) {
    if (err) {
      console.log(new Date(), "Non HTTP STATUS error");
      console.log(JSON.stringify(err, null, "\t"));
      resolve({ status: "-1" });
    } else if (res.statusCode > HTTP_STATUS.OK) {
      resolve({
        url: res.request.href,
        status: res.statusCode,
        method: res.request.method,
        body: formResponse(res.body)
      });
    } else {
      resolve({
        url: res.request.href,
        body: formResponse(body),
        status: res.statusCode,
        method: res.request.method,
        headers: res.headers
      });
    }
  };
};

module.exports = resolveHandler;
